<?php
function getLayerIDbyName($layer_url, $layer_name) {
    $json = file_get_contents_curl($layer_url.'?f=json');
    $MapServerJSON = json_decode($json, true);
    $r = '';
    foreach ($MapServerJSON['layers'] as $layer) {
        if ($layer['name'] == $layer_name) {
           $r = $layer['id'];
        }
    }
    return $r;
}

function getLayersConfig() {
    $configJSON = json_decode(file_get_contents_curl('http://map.geog.ucsb.edu/data/layers.json.php?layers=all'), true);
    foreach ($configJSON as $layer) {
        $layersConfig[$layer['id']]=$layer;
    }
    return $layersConfig;
}
?>