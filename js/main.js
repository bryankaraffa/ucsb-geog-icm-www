/* Import required JavaScript libraries */
dojo.require("dojo._base.array");
dojo.require("dojo.json");
dojo.require("dijit.layout.BorderContainer");
dojo.require("dijit.layout.ContentPane");
dojo.require("dijit.layout.AccordionContainer");
dojo.require("dijit.layout.TabContainer");
dojo.require("dijit.form.Form");
dojo.require("dijit.form.TextBox");
dojo.require("dijit.Toolbar");
dojo.require("dijit.Tree");
dojo.require("dijit.Dialog");
dojo.require("dijit.form.Button");
dojo.require("esri.map");
dojo.require("esri.dijit.Scalebar");
dojo.require("esri.dijit.Legend");
if (user_device == 'mobile' || user_device == 'tablet') {
    dojo.require("esri.dijit.PopupMobile");
}
else {
    dojo.require("esri.dijit.Popup");
}
dojo.require("esri.dijit.InfoWindow");
dojo.require("dojo.data.ItemFileReadStore"); // Needed by Search
dojo.require("dijit.form.FilteringSelect"); // Needed by Search
dojo.require("esri.tasks.query"); // Needed by Parking Infowindows
dojo.require("esri.layers.KMLLayer"); // Needed to support KML layers
    
/* Define Global Variables */
var map;
var layers = new Array();  	//Holds the initialized layers.
var layersActive = new Array();	//Holds the visible layers.

var legendLayers = new Array();	//Holds active layers for legend
var legend = new Array();       //Legend Object

var benchmark = [];
benchmark['startTime'] = new Date().getTime();

/* Function init() starts when dojo is loaded */
function init() {
	
	campusExtent = new esri.geometry.Extent({xmin: -13342700,ymin: 4084100,	xmax: -13339988, ymax: 4084700,spatialReference:{wkid:102100}});
	
        if (user_device == 'mobile'  || user_device == 'tablet') {
            var popup = new esri.dijit.PopupMobile(null, dojo.create("div"));
            
            //Start Geolocation if on Mobile or tablet.

        }
        else {
            var popup = new esri.dijit.Popup(null, dojo.create("div"));
        }
	map = new esri.Map("mapDiv", { 
		extent: campusExtent,
		infoWindow: popup
	});
	var scalebar = new esri.dijit.Scalebar({
	  map:map,
	  attachTo:"bottom-left"
	});
	
	dojo.connect(map,"onUpdateEnd",hideLoading);
	dojo.connect(map,"onUpdateStart",showLoading);
	
	initLegend();
	initLayerLoader();
	//initRoomBuildingFinder();
		
	ddtreemenu.createTree("layersmenu", true);
	ddtreemenu.flatten('layersmenu', 'expand')
	
	

	/* When map loads, run these commands */
	dojo.connect(map, 'onLoad', function(theMap) {		
		//Once the map has loaded, initialize the onClick-Parking infowindows
		initParkingPopup();
		
		//Once the map has loaded, init the onClick-Building infowindows
		initBuildingPopup();
		
		//Once the map has loaded, init the Print Module
		//initPrintModule(); //Disabled -- broken as of 03/2014 ArcGIS API update broke it.
		
		//Once the map has loaded, init the Search Module (version 2)
		initSearchModule_v2();	
                
                initGeolocation(map);
                initBasemapGallery();
		
			
		//resize the map when the browser resizes
		dojo.connect(dijit.byId('mapDiv'), 'resize', map,map.resize);
		
		//update extent on certain page elements when extent is changed
		//This includes the Share Link box and scale indicator.
		dojo.connect(map, "onExtentChange", onMapExtentChange);
		
		document.getElementById("info").value = mapRootUrl;
		
		//Update the Map Extent once the page and map have been loaded. 
		onMapExtentChange(map.extent, map._delta, '', map.__LOD);
                
                if (user_device == 'mobile') {
                    window.toggleSidebar();
                }
                //Once the map has loaded and added the basemap, check to see if the user is using any parameters from a shared link
		initShareLink();
		
	}); //end of dojo.connect(map, 'onLoad');
  	

} // End of init();


dojo.addOnLoad(init);


///////////////////////////////////////////
/*      Functions and Includes           */
///////////////////////////////////////////

function hideLoading() {
	benchmark['startTime'] = new Date().getTime();
	document.getElementById('loadingImg').style.display = 'none';
}
function showLoading() {
	document.getElementById('loadingImg').style.display = 'block';
	benchmark['endTime'] = new Date().getTime();
	console.log('Load Time: '+(benchmark['endTime'] - benchmark['startTime'])+'ms.');
}
function initLegend() {
	dojo.connect(map,'onLayersAddResult',function(results){
	  if (legend._started) {
	  	legend.refresh();
	  }
	  else {	  	
		  legend = new esri.dijit.Legend({
		    map:map,
		    layerInfos: legendLayers,
		    respectCurrentMapScale:true
		  },"legendDiv");
		  legend.startup();
		  console.log('Legend module started.');
	  }

	});
}
function orientationChanged() {
	if(map) {
        map.reposition();
    	map.resize();
	}
}
// `cleanArray(actual)` - Returns an array with null & false values removed.
function cleanArray(actual){
  var newArray = new Array();
  for(var i = 0; i<actual.length; i++){
      if (actual[i]){
        newArray.push(actual[i]);
    }
  }
  return newArray;
}

// `getLayerIDbyName(layer,layer_name)` - Returns layerid (*type: int*) for a layer given a layer_name string.  `layer` is a unique layerid as specified in the MySQL `layers` table, and `layer_name` is a string, i.e. "Buildings".  Used for finding which layer the Buildings/Parking Layer is in the Basemap MapService
function getLayerIDbyName(layer,layer_name) {
    var r = '';
    dojo.forEach(layers[layer].layerInfos, function(lyr){
        if (lyr.name == layer_name) {
           r=lyr.id;
        }
    });
    return r;
}

// ON MAP EXTENT CHANGE
function onMapExtentChange(extent, delta, levelChange, lod){
	//var scale = dojo.number.format(lod.scale, {
	//	places: 0
	//});
	//dojo.byId('mapScale').innerHTML = dojo.string.substitute("1 : ${0}", [scale]);
	if (sharelinkDialog.open == false) {
            updateShareLink();
        }
	console.log('Extent updated in share link.');
        
        if (lod.scale <= 2000 && dojo.byId("floorSelectorDiv").style.display != 'block') {
            dojo.byId("floorSelectorDiv").style.display='block';
            console.log("Displaying the floor selector.");
        }
        else if (lod.scale > 2000 && dojo.byId("floorSelectorDiv").style.display != 'none') {
            dojo.byId("floorSelectorDiv").style.display='none';
            console.log("Hiding the floor selector.");
        }
}
function SelectAll(id) {
	document.getElementById(id).focus();
	document.getElementById(id).select();
}
function parseGeometry(result_geometry) {
    var geometry = '';
    result_geometry = eval(result_geometry);
    if (result_geometry[0].length > 1) {
        var geometry = new esri.geometry.Polygon(new esri.SpatialReference({wkid:102100})); 
        geometry.addRing(result_geometry[0]);
    }
    else {
        if ((typeof result_geometry[0]['x'] === 'number') && (typeof result_geometry[0]['y'] === 'number')) {
            var geometry = new esri.geometry.Point([result_geometry[0]['x'],result_geometry[0]['y']],new esri.SpatialReference({wkid:102100}));        }
    }

    return geometry;
}