var hoverGraphicsLayerBuilding;

function initBuildingPopup() {
		if (window.location.hostname === '11.11.11.11') {
			var mapRootUrl = 'http://map.geog.ucsb.edu/';
		}
		else { var mapRootUrl = ''; }
		
	
		dojo.connect(map.infoWindow,"onMaximize",function(){
			resizeBuildingPic('full')
			console.log('Info Window Maximized');
		});
		dojo.connect(map.infoWindow,"onRestore",function(){
			resizeBuildingPic('thumb');
			console.log('Info Window Restored to default state.');
		});
			
		hoverGraphicsLayerBuilding = new esri.layers.GraphicsLayer({id:'infowindow_buildings'});
		var buildingLayer = getLayerIDbyName('basemap','Buildings');
        var queryUrl = checkQueryURL(layersConfig['basemap'])+"/"+buildingLayer;
                
		//build query filter
		var queryTask = new esri.tasks.QueryTask(queryUrl);  // "/9" is the 9th layer on the MapServer
		var queryObject = new esri.tasks.Query();
		queryObject.returnGeometry = true;
		queryObject.outFields = ["bl_num", "b_name", "department"];
		queryObject.where = "1=1";
		//queryObject.geometry = evt.mapPoint;  //query the clicked point

		var someText = "<b>Building #:</b> ${bl_num}<a href='" + mapRootUrl + "images/i/bldg_${bl_num}/?ref=bldg_infowindow' target='_blank'><img id='infoWindow_bldgPic' align='right' src='" + mapRootUrl + "images/i/bldg_${bl_num}/thumb/?ref=bldg_infowindow' style='float: right; margin: 2px; max-width:50%; min-width:0px; min-height:0px;'></a><p>${department}</p><p><a href='#' onclick='loadEnergyUsage(${bl_num}, \"${b_name}\")'><img src='images/iconChart_small.png' style='padding-right:5px'/>View Energy Usage</a></p>";

		var infoTemplate = new esri.InfoTemplate();
		infoTemplate.setTitle("${b_name}<img src='/images/trans1x1.png'></img>");
		infoTemplate.setContent(someText);

		dojo.connect(queryTask, "onComplete", function(featureSet4) {
			 
			var symbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0,0,0,0])), new dojo.Color([0,0,128,0]));
			var highlightSymbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([115,255,223]), 3), new dojo.Color([115,225,223,0.05]));

			//QueryTask returns a featureSet.  Loop through features in the featureSet and add them to the map.
			for (var i=0, il=featureSet4.features.length; i<il; i++) {
				//Get the current feature from the featureSet, Feature is a graphic
				var graphic4 = featureSet4.features[i];
				
				graphic4.setSymbol(symbol);
				graphic4.setInfoTemplate(infoTemplate);
				
				if(graphic4.attributes.bl_num == null){
					graphic4.attributes.bl_num = 'unidentified';			
				}
				if(graphic4.attributes.department != null && graphic4.attributes.department!= " " && graphic4.attributes.department != ""){
					var deptString = graphic4.attributes.department;
					var deptArray = deptString.split(';');
					var departments = '<b>Departments:</b> <br />';
					for ( var z=0, len=deptArray.length; z<len; ++z ){
						  departments +="<a href=\""+getDeptUrl(deptArray[z])+"\" target=\"_blank\">";
						  departments +=deptArray[z]+"</a><br />";
					}
					graphic4.attributes.department = departments;
				}
				
				//Add graphic to the floor graphics layer.
				hoverGraphicsLayerBuilding.add(graphic4);
			}
			map.addLayer(hoverGraphicsLayerBuilding);
			map.graphics.enableMouseEvents();
			//click on hoverGraphicsLayerBuilding creates new graphic with geometry from the event.graphic and adds it to the maps graphics layer
			dojo.connect(hoverGraphicsLayerBuilding, "onClick", function(evt) {
				map.infoWindow.resize(350,450);
				map.graphics.clear();  //use the maps graphics layer as the highlight layer
				var content = evt.graphic.getContent();
				var title = evt.graphic.getTitle();
				var highlightGraphic = new esri.Graphic(evt.graphic.geometry,highlightSymbol);
				map.graphics.add(highlightGraphic);
                                if (user_device == 'mobile' || user_device == 'tablet') {
                                    map.infoWindow.setFeatures([evt.graphic]);
                                }
			});
			dojo.connect(hoverGraphicsLayerBuilding, "onMouseOver", function(evt) {
				evt.graphic.setSymbol(highlightSymbol);
				map.setCursor("pointer");
			});
			dojo.connect(hoverGraphicsLayerBuilding, "onMouseOut", function(evt) {
				map.graphics.clear();
				evt.graphic.setSymbol(symbol);
				map.setCursor("default");
			});
		});
		queryTask.execute(queryObject);
}

function resizeBuildingPic(size) {
	var picUrl = document.getElementById('infoWindow_bldgPic').src;
	if (size == 'full') {
		var newUrl = picUrl.replace( "_Thumbs", "_Resized" );		
	}
	if (size == 'thumb') {
		var newUrl = picUrl.replace( "_Resized", "_Thumbs" );		
	}
	document.getElementById('infoWindow_bldgPic').src = newUrl;
}
//Get Building URL
	function getDeptUrl(dept) {
			switch (dept) {
				 case 'Anthropology': return("http://www.anth.ucsb.edu/"); 
							 break; 
				 case 'Asian American Studies': return("http://www.asamst.ucsb.edu/"); 
							 break; 
				 case 'Classics': return("http://www.classics.ucsb.edu/"); 
							 break; 
				 case 'East Asian Languages and Cultural Studies': return("http://www.eastasian.ucsb.edu/"); 
							 break; 
				 case 'History': return("http://www.history.ucsb.edu/"); 
							 break; 
				 case 'Religious Studies': return("http://www.religion.ucsb.edu/"); 
							 break; 
				 case 'Art': return("http://www.arts.ucsb.edu/"); 
							 break; 
				 case 'Biomolecular Science and Engineering': return("http://www.bmse.ucsb.edu/"); 
							 break; 
				 case 'Biological Sciences': return("#"); 
							 break; 
				 case 'Ecology, Evolution and Marine Biology': return("http://lifesci.ucsb.edu/EEMB/"); 
							 break; 
				 case 'Marine Science': return("http://marinegp.ucsb.edu/"); 
							 break; 
				 case 'Molecular, Cellular and Developmental Biology': return("http://lifesci.ucsb.edu/MCDB/"); 
							 break; 
				 case 'Black Studies': return("http://www.blackstudies.ucsb.edu/"); 
							 break; 
				 case 'Chicano Studies': return("http://www.chicst.ucsb.edu/"); 
							 break; 
				 case 'English': return("http://www.english.ucsb.edu/"); 
							 break; 
				 case 'Feminist Studies': return("http://www.femst.ucsb.edu/"); 
							 break; 
				 case 'Linguistics': return("http://www.linguistics.ucsb.edu/"); 
							 break; 
				 case 'Mathematics': return("http://www.math.ucsb.edu/ie_index.html"); 
							 break; 
				 case 'Philosophy': return("http://www.philosophy.ucsb.edu/"); 
							 break; 
				 case 'Statistics': return("http://www.pstat.ucsb.edu/"); 
							 break; 
				 case 'Writing': return("http://www.writing.ucsb.edu/"); 
							 break; 
				 case 'Chemical Engineering': return("http://www.chemengr.ucsb.edu/"); 
							 break; 
				 case 'Materials': return("http://www.materials.ucsb.edu/"); 
							 break; 
				 case 'Mechanical Engineering': return("http://www.me.ucsb.edu/"); 
							 break; 
				 case 'Communication': return("http://www.comm.ucsb.edu/"); 
							 break; 
				 case 'Film and Media Studies': return("http://www.filmandmedia.ucsb.edu/"); 
							 break; 
				 case 'Global and International Studies': return("http://www.global.ucsb.edu/"); 
							 break; 
				 case 'Sociology': return("http://www.soc.ucsb.edu/"); 
							 break; 
				 case 'Comparative Literature': return("http://www.complit.ucsb.edu/"); 
							 break; 
				 case 'French and Italian': return("http://www.frit.ucsb.edu/"); 
							 break; 
				 case 'Germanic, Slavic, and Semitic Studies': return("http://www.gss.ucsb.edu/"); 
							 break; 
				 case 'Media Arts and Technology': return("http://www.mat.ucsb.edu/"); 
							 break; 
				 case 'Spanish and Portugese': return("http://www.spanport.ucsb.edu/"); 
							 break; 
				 case 'Computer Science': return("http://www.cs.ucsb.edu/"); 
							 break; 
				 case 'Electrical Engineering': return("http://www.ece.ucsb.edu/"); 
							 break; 
				 case 'Counseling, Clinical, and School Psychology': return("http://education.ucsb.edu/Graduate-Studies/CCSP/CCSP-home.html"); 
							 break; 
				 case 'Education': return("http://education.ucsb.edu/Graduate-Studies/Education/home.htm"); 
							 break; 
				 case 'Dance': return("http://www.theaterdance.ucsb.edu/"); 
							 break; 
				 case 'Earth Science': return("http://www.geol.ucsb.edu/"); 
							 break; 
				 case 'Economics': return("http://www.econ.ucsb.edu/"); 
							 break; 
				 case 'Environmental Studies': return("http://www.es.ucsb.edu/"); 
							 break; 
				 case 'Bren School': return("http://www.bren.ucsb.edu/"); 
							 break; 
				 case 'Geography': return("http://www.geog.ucsb.edu/"); 
							 break; 
				 case 'Political Science': return("http://www.polsci.ucsb.edu/"); 
							 break; 
				 case 'History of Art and Architecture': return("http://www.arthistory.ucsb.edu/"); 
							 break; 
				 case 'Military Science (ROTC)': return("http://www.milsci.ucsb.edu/"); 
							 break; 
				 case 'Music': return("http://www.music.ucsb.edu/"); 
							 break; 
				 case 'Physics': return("http://www.physics.ucsb.edu/"); 
							 break; 
				 case 'Psychological and Brain Science': return("http://www.psych.ucsb.edu/"); 
							 break; 
				 case 'Theater': return("http://www.theaterdance.ucsb.edu/"); 
							 break; 
				}
			}