var placeStore, searchFS, bldgFS, roomFS;

function initRoomBuildingFinder() {
		//Preload path to JSON for search pane, and room locator pane
		placeStore = new dojo.data.ItemFileReadStore({
		url: "/data/search5.json.txt",
		typeMap: { 'Extent': esri.geometry.Extent}
		});
		
		 
		var searchFS = new dijit.form.FilteringSelect({
			name: "place", 
			required: false, 
			store: placeStore, 
			searchAttr: "name", 
			autoComplete:false, 
			queryExpr:"*${0}*"}, "placeSelect");
		dojo.connect(dijit.byId("placeSelect"), "onChange", searchZoomer);
		dojo.connect(dijit.byId("placeSelect"), "onKeyPress", function(evt){
			if(evt.charOrCode==dojo.keys.ENTER) {
				searchZoomer();
			}
		});

		bldgStore = new dojo.data.ItemFileReadStore({
			url: "/js/bldgs2.json.txt", 
			typeMap: { 'Extent': esri.geometry.Extent}
		});

		roomStore = new dojo.data.ItemFileReadStore({
			url: "/js/rooms2.json.txt", 
			typeMap: { 'Extent': esri.geometry.Extent}
		});

		//populate the buildings/room combo box with the building names
		var bldgFS = new dijit.form.FilteringSelect({ 
			name: "bldg", 
			required: false,
			store: bldgStore, 
			searchAttr: "name",
			autoComplete:false,
			queryExpr:"*${0}*"}, "bldgSelect"); 
		//zoom to the extent for a building or room after a selection is made or if enter key pressed
		dojo.connect(dijit.byId("bldgSelect"), "onChange", bldgZoomer);
		dojo.connect(dijit.byId("bldgSelect"), "onKeyPress", function(evt){
			if(evt.charOrCode==dojo.keys.ENTER) {
				bldgZoomer();
			}
		});

		//populate the buildings/rooms combo box with the room names
		var roomsFS = new dijit.form.FilteringSelect({
			store: roomStore,
			disabled: true,
			displayedValue: "Please select a building.",
			label: "name",
			name: "rooms", 
			required: false, //disables warnings about invalid input
			searchAttr: "name"
			}, "roomSelect"); 
		dojo.connect(dijit.byId("roomSelect"), "onChange", roomZoomer);
		dojo.connect(dijit.byId("roomSelect"), "onKeyPress", function(evt){
			if(evt.charOrCode==dojo.keys.ENTER) {
				roomZoomer();
			}
		});
}

function roomZoomer() {
	var roomFips = dijit.byId('roomSelect').attr('value');
	var roomNumber;
	var bldgName;
	if (roomFips != "") {
		//query the data store to get the extent and set the map extent equal to the extent from the store
		roomStore.fetchItemByIdentity({
			identity: roomFips, 
			onItem: function(item, request) {
				roomNumber = roomStore.getValue(item, "name") //only needed for linkRoomText
				bldgName = roomStore.getValue(item, "bldg_name") //only needed for linkRoomText
				newExtent=esri.geometry.geographicToWebMercator(item.extent[0]);
				showExtentRoom();
			},
			onError: function(item, request) {
				oops();
			}
		});
	}

	var linkRoomText=mapRootUrl+"?bldg=" + escape(bldgName) + "&room=" + escape(roomNumber);
	document.getElementById("linkRoom").value = linkRoomText;
}

function searchZoomer() {
	var placeName = dijit.byId('placeSelect').attr('value');
	placeStore.fetchItemByIdentity({
		identity: placeName, 
		onItem: function(item, request) {
			newExtent=esri.geometry.geographicToWebMercator(item.extent[0]);
			showExtent();			
		},
		onError: function(item, request) {
			oops();
		}
	});
}

function bldgZoomer() {  
	var bldgName = dijit.byId('bldgSelect').attr('value');
	if (bldgName != "") {
		//dijit.byId(this.id + '.ddDialog').onCancel(); //this will close the drop down button
		dijit.byId('roomSelect').query = {bldg_name: bldgName};
		//dijit.byId('roomSelect').store = roomStore;

		bldgStore.fetchItemByIdentity({
			identity: bldgName, 
			onItem: function(item, request) {
				newExtent=esri.geometry.geographicToWebMercator(item.extent[0]);
				showExtent();
				dijit.byId('roomSelect').attr({ disabled: false, displayedValue: '' });
				dijit.byId('roomSelect').setDisplayedValue('');
				dijit.byId("roomSelect").focus(); //give focus to the room filtering select
			},
			onError: function(item, request) {
				oops();
			}
		});
	}
}
	function showExtent() {
		if (newExtent[0] && typeof(newExtent[0].geometry) != 'undefined') { 
			var ext = newExtent[0].geometry; //from a geom service
		} 
		else { 
			var ext = newExtent; //straight from the json file
		}

		//create a copy of the extent because sending the extent to map.setExtent() alters 
		//the extent passed to it which screws up the graphic that we draw
		var newMapExtent = dojo.clone(ext); 
		map.setExtent(newMapExtent.expand(4), true); //change the expand factor to adjust zoom level

		var opac = 1;
		//don't start the fade out right away so the map has time to update
		setTimeout(function() { 
			var outline = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 215, 0, 1]), 7);
			var symbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, outline, new dojo.Color([255, 215, 0, 0.5]));
			var extGraphic = new esri.Graphic(ext, symbol);
			map.graphics.add(extGraphic);
			var i_id = setInterval(function() { //fade out the graphic representing the new extent
				//if you want a different color for the extent graphic, alter the rgb values below
				symbol.outline.setColor(new dojo.Color([255, 215, 0, opac])); 
				extGraphic.setSymbol(symbol);
				if (opac < 0.01) { //once the graphic is no longer visible, clear the interval, remove graphic
				clearInterval(i_id); 
				map.graphics.remove(extGraphic);
				}
				opac -= 0.01;
			}, 50);
		}, 1000); //delay in milliseconds
	}

	function showExtentRoom() {
		if (newExtent[0] && typeof(newExtent[0].geometry) != 'undefined') { 
			var ext = newExtent[0].geometry; //from a geom service
		} 
		else { 
			var ext = newExtent; //straight from the json file
		}

		//create a copy of the extent because sending the extent to map.setExtent() alters 
		//the extent passed to it which screws up the graphic that we draw... 
		var newMapExtent = dojo.clone(ext);
		map.setExtent(newMapExtent.expand(28));

		var outline = new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([255, 215, 0, 1]), 3);
		var symbol = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, outline, new dojo.Color([255, 215, 0, 0.5]));
		var extGraphic = new esri.Graphic(ext, symbol);

		var gLayer = new esri.layers.GraphicsLayer(); 
		//gLayer.id = 'layer1'; //this little line creates a big bug in basemapgallery functionality
		map.addLayer(gLayer);
		var features = [];
		features.push(extGraphic);
		var featureSet = new esri.tasks.FeatureSet();
		featureSet.features = features;
		gLayer.add(featureSet.features[0]);

		dojo.connect(gLayer, "onClick", function(evt) {
			gLayer.remove(extGraphic);
		});

		dojo.connect(dijit.byId("roomSelect"), "onChange", function(evt) {
			gLayer.remove(extGraphic);
		});
	}