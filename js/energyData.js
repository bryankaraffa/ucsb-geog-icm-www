var energyData = new Array;
function loadEnergyUsage(bnum, bname) {
	var url = "/api.php?action=getEnergyData&returnOnlyData=true&ts="+ (new Date().getTime()) +"&bldgNumber="+bnum;
	var data = dojo.xhrGet({
	    // The URL to request
	    url: url,
	    handleAs: "json",
	    // The method that handles the request's successful result
	    // Handle the response any way you'd like!
	    load: function(data){
	    	if (data != null) { 
	    		drawEnergyChart(data, bname, bnum);
	    		energyData[bnum] = new Array;
	    		energyData[bnum]['bname']=bname;
	    		energyData[bnum]['bnumber']=bnum;
	    		energyData[bnum]['data']=data;
	    	}
	    	else { alert('Energy data current not available for '+bname+'.\nPlease check back later.'); }
	    },
	    error: function(error){
	      alert("An unexpected error occurred: " + error);
	    }
	});
	// This needs to be moved to it's own module
	//var pane = dijit.byId('pane_EnergyUsage');
	//dojo.byId('visualization').innerHTML = 'Loading Energy Data...'
	//dijit.byId('mainAccordionCont').selectChild(pane);	
	//dojo.byId('energySub').innerHTML = "Energy Usage for "+bname+"<br/><br/><span='font-size:0.9em'>This functionality is currently underconstruction, but check back soon to see monthly electric, gas and water usage for each building.<br/><br/>If you play your cards right, we might even have some 24 hour data for you!<br/><br/><img width='100%' src='images/sampleChart.png'/></span>";
}
// Energy Module
function drawEnergyChart(data, bname, bnum) {
	energydashboardDialog.show();
	$(function() {
		// Create the chart
		window.chart = new Highcharts.StockChart({
			chart : {
				renderTo : 'energycontainer',
				backgroundColor : '#F7F7F7'
			},

			rangeSelector : {
				selected : 4
			},

			title : {
				text : bname + ' Electricity Usage (kWh)',
				align: 'left'
			},
			
			series : [{
				name : 'Electricity Usage (kWh)',
				data : data,
				tooltip: {
					valueDecimals: 0
				}
			}],
			yAxis : [{
				min:0,
				minorTickInterval: 'auto'
			}],
			xAxis : [{
				gridLineWidth: 1,
				lineColor: '#000',
				tickColor: '#000',
				tickInterval: 30 * (24 * 3600 * 1000)  //30 * (1 day in seconds)
			}]
	});
	//map.infoWindow.resize(350,550);
});
}