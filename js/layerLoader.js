var layers = [];  		//Holds the initialized layers.
var layersActive = [];	//Holds the visible layers.
var layersConfig = [];
var infowindowGraphicsLayer = [];

function initLayerLoader() {
	var api_url = "/data/layers.json.php";
	dojo.xhrGet({
	    // The URL to request
	    url: api_url,
	    handleAs: "json",
	    // The method that handles the request's successful result
	    // Handle the response any way you'd like!
	    load: gotLayersJSON
	});
}
function gotLayersJSON(layersJSON) {
	console.log('Got layers.json.');
	dojo.forEach(layersJSON, function(layer) {
		layersConfig[layer.id] = layer;
	//	layers[layer.id] = initJSONLayer(layer);
		if (layer.id == 'basemap') {
			addLayer('basemap');
		}
	});
}
function addLayer(layer) {
	if(typeof layers[layer] == 'undefined' || typeof layers[layer].loaded != 'boolean') {
		if (typeof layersConfig[layer] == 'undefined') {
			var api_url = "/data/layers.json.php";
			console.log(layer + ' was not included in the default layers config.  Will attempt to load it dynamically.');
			dojo.xhrGet({
			    // The URL to request
			    url: api_url+"?layers="+layer,
			    handleAs: "json",
			    // The method that handles the request's successful result
			    // Handle the response any way you'd like!
			    sync: true,
			    load: gotLayersJSON
			});
		}
		layers[layer] = initJSONLayer(layersConfig[layer]);
	}
	if (dojo.indexOf(layersActive,layer) == -1) { // check if layer is already active.
			map.addLayers([layers[layer]]);
			
			if (layers[layer].id != 'basemap') {
				if (layers[layer].id == 'SBMTD' || layers[layer].id == 'webcams') {
					layers['esriBasemapCanvas'] = new esri.layers.ArcGISTiledMapServiceLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer', {id:'esriBasemapCanvas'});
					layers['esriBasemapCanvasRef'] = new esri.layers.ArcGISTiledMapServiceLayer('http://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Reference/MapServer', {id:'esriBasemapCanvas'}); 
					console.log('Adding ESRI Grey Basemap');
					map.addLayer(layers['esriBasemapCanvas'], 0);
					console.log('Adding ESRI Grey Basemap Reference');
					map.addLayer(layers['esriBasemapCanvasRef'],1);
				}
				legendLayers.push({layer:layers[layer], title:' '});
			}
			else {
				map.addLayer(layers[layer]);
			}
			//map.addLayers([layers[layer]]);
			layers[layer].setVisibility(true);
			if (layersConfig[layer].infowindow != null) {
				//layersConfig[layer].infowindow = dojo.json.parse(layersConfig[layer].infowindow);
				
				var count = 0;
				dojo.forEach(layersConfig[layer].infowindow, function(iw){
					initLayerPopup(layer, count);
					count++;
				});
			}
			console.log('Activated "'+layers[layer].id+'".');
			//Adds the newly active layer id to the top of the activeLayers array
			layersActive.unshift(layers[layer].id);
	}
	else {
		console.log('Layer "'+layer+'" already active.');
	}
	updateShareLink();
	
	dojo.connect(layers[layer], "onResume", function(result) { 
		if (layersConfig[layer].zoomOnLoad == 1) {
				map.setExtent(layers[layer].fullExtent.expand(1.5));	
		}
                if (layersConfig[layer].zoomOnLoad == 2) {
				map.setExtent(campusExtent);
		}
		if (typeof layersConfig[layer].mapOverlay != null && layersConfig[layer].mapOverlay != '') {
			//alert('Setting Map Overlay');
			dojo.byId('mapOverlayDiv').style.visibility = 'visible';
			dojo.byId('mapOverlayDiv').innerHTML = layersConfig[layer].mapOverlay;	
		}
		else {
			dojo.byId('mapOverlayDiv').style.display = 'hidden';
			dojo.byId('mapOverlayDiv').innerHTML = '';
		}
		if (layersConfig[layer].openLegendOnLoad == 1) {
			dijit.byId('mainAccordionCont').selectChild(dijit.byId('pane_Legend'));
		}
		if (legend._started) {
			legend.refresh();	
		}
	});
}
function initJSONLayer(json) {
	if (typeof json == "undefined") { return false; }
	// Verify value for opacity, default to 1 (full opacity) if there are errors
	if (typeof json.opacity == undefined || isNaN(json.opacity) || json.opacity == null) {
		json.opacity = 1;
	}
	
	if (json.type == 'dynamic') {
		var lyr = new esri.layers.ArcGISDynamicMapServiceLayer(json.url, {id:json.id, opacity:json.opacity});
		}		
	if (json.type == 'featureservice') {
		dojo.require("esri.layers.FeatureLayer");
		var lyr = new esri.layers.FeatureLayer(json.url, {id:json.id, opacity:json.opacity, mode: esri.layers.FeatureLayer.MODE_ONDEMAND});
		}
	if (json.type == 'cached') { 
		var lyr = new esri.layers.ArcGISTiledMapServiceLayer(json.url, {id:json.id, opacity:json.opacity});
		}
	if (json.type == 'kml') { 
		var jsonURL = json.url+'?nocache='+(new Date).getTime();
		var lyr = new esri.layers.KMLLayer(jsonURL, {id:json.id, opacity:json.opacity}); 
		}
	
	console.log('Layer "'+json.id+'" initialized from layers.json."');

	return lyr;
	
}
function initLayerPopup(layer, infowindow) {
		infowindowGraphicsLayer[layer] = new esri.layers.GraphicsLayer();
		
		
		//build query filter
		if (parseInt(layersConfig[layer].infowindow[infowindow].layerID) >= 0) {
			var parkingQueryTask = new esri.tasks.QueryTask(checkQueryURL(layersConfig[layer]) + "/" + layersConfig[layer].infowindow[infowindow].layerID);
		}
		else {
			var parkingQueryTask = new esri.tasks.QueryTask(layersConfig[layer].infowindow[infowindow].layerID);
		}
		
		var parkingQuery = new esri.tasks.Query();
		parkingQuery.returnGeometry = true;
		parkingQuery.outFields = ["*"];
		parkingQuery.where = "1=1";
		parkingQuery.geometryPrecision = 1;
		//parkingQuery.geometry = evt.mapPoint;  //query the clicked point

		var infoTemplate = new esri.InfoTemplate();
		infoTemplate.setTitle(layersConfig[layer].infowindow[infowindow].infowindowTitle);
		infoTemplate.setContent(layersConfig[layer].infowindow[infowindow].infowindowContent);

		dojo.connect(parkingQueryTask, "onComplete", function(featureSet4) {

			//QueryTask returns a featureSet.  Loop through features in the featureSet and add them to the map.
			for (var i=0, il=featureSet4.features.length; i<il; i++) {			
				//Get the current feature from the featureSet, Feature is a graphic
				var graphic4 = featureSet4.features[i];
				if (featureSet4.geometryType == 'esriGeometryPolygon') {
					var symbolParking = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0,0,0,0])), new dojo.Color([0,0,128,0]));
					var highlightSymbolParking = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0,0,128]), 2), new dojo.Color([0,0,128,0.4]));				
				}
				if (featureSet4.geometryType == 'esriGeometryPoint') {
					var symbolParking = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE, 30,new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID,   new dojo.Color([0,0,0, 0]), 2),   new dojo.Color([255,255,255,0]));
					var highlightSymbol = new esri.symbol.SimpleMarkerSymbol(esri.symbol.SimpleMarkerSymbol.STYLE_CIRCLE, 30,new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID,   new dojo.Color([0,0,0, 0]), 2),   new dojo.Color([255,255,255,0]));
				}	
				graphic4.setSymbol(symbolParking);
				graphic4.setInfoTemplate(infoTemplate);
				//Add graphic to the floor graphics layer.
				infowindowGraphicsLayer[layer].add(graphic4);
			}
			map.addLayer(infowindowGraphicsLayer[layer]);
			map.graphics.enableMouseEvents();
			//click on infowindowGraphicsLayer[layer] creates new graphic with geometry from the event.graphic and adds it to the maps graphics layer
			dojo.connect(infowindowGraphicsLayer[layer], "onClick", function(evt) {
				//map.infoWindow.resize(350,250);
				map.graphics.clear();  //use the maps graphics layer as the highlight layer
				var content = evt.graphic.getContent();
				var title = evt.graphic.getTitle();
                                //dojo.byId("mapInfowindowDiv").style.visibility = 'visible';
                                //dojo.byId("mapInfowindowDiv").innerHTML = title + '<hr />' + content;
                                if (user_device == 'mobile' || user_device == 'tablet') {
                                    map.infoWindow.setFeatures([evt.graphic]);                     
                                }    
            
            
				//var highlightGraphic = new esri.Graphic(evt.graphic.geometry,highlightSymbolParking);
				//map.graphics.add(highlightGraphic);
			});
			dojo.connect(infowindowGraphicsLayer[layer], "onMouseOver", function() {
				map.setCursor("pointer");
			});
			dojo.connect(infowindowGraphicsLayer[layer], "onMouseOut", function() {
				map.setCursor("default");
			});
		});
		parkingQueryTask.execute(parkingQuery);
}
function clearLayers() {
	dojo.byId('mapOverlayDiv').innerHTML = '';
	dojo.byId('mapOverlayDiv').style.visibility = 'hidden';
	if (layersActive.length > 0) {  //if there are active layers, start the loop
		dojo.forEach(layersActive, function(layer) {
			//Prevent the basemap from being disabled.
			if (layers[layer].id != 'basemap') {
				layers[layer].setVisibility(false);	
				if (infowindowGraphicsLayer[layer]) {
					infowindowGraphicsLayer[layer].clear();
				}
			}
		});
		layersActive = [];
		legendLayers.length = 0;  //Reset the legendLayers array so that the legend doesn't add duplicate entries.'
	}
	if (layers['esriBasemapCanvas'] || layers['esriBasemapCanvasRef']) {
		map.removeLayer(layers['esriBasemapCanvas']);
		map.removeLayer(layers['esriBasemapCanvasRef']);
	}
        if (typeof searchresultGraphicLayer === 'object') {
            map.removeLayer(searchresultGraphicLayer);
            //setTimeout(map.addLayer(searchresultGraphicLayer),1000);
        }
	if (legend._started) {
		legend.refresh();	
	}
        searchResultClicked = '';
}
function changeLayer(layer) {
	//if (typeof layers[layer] == undefined) {
		//console.log('Layer not initialized.  Loading layer: '+layer);
	//	alert('First time initializing layer..............');
	//}
	if (dojo.indexOf(layersActive,layer) == -1 || layer === 'customlayer') {
		clearLayers();
		setTimeout(addLayer(layer), 1000);
		 
		//console.log('Zooming to layer extent: '+layers[layer].initialExtent);
		//map.setExtent(layers[layer].fullExtent);
                dojo.xhrPost({
                    url: "/api.php",
                    content: {
                      action: "layerLoaded",
                        layer: layer
                    }
                });
	}
	else {
		console.log('Layer "'+layers[layer].id+'" already active.');
	}
	if (legend._started) {
		legend.refresh();	
	}
}
function checkQueryURL(layer_config) {
	if (typeof layer_config.query_url !== 'string' || layer_config.query_url.length === 0) {
		return layer_config.url;
	}
	else {
		return layer_config.query_url;
	}
}
