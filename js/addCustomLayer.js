function addCustomLayer() {
	var customURL = document.getElementById("addcustomlayer_URL").value;
	var customType = document.getElementById("addcustomlayer_Type").value;
	var customJSON = dojo.fromJson('{"id": "customlayer","url": "'+ customURL +'","type": "' + customType +'", "opacity": 1}');
	layersConfig['customlayer']=customJSON;
	console.log(checkCustomURLType(customURL));
	//layers['customlayer'] = initJSONLayer(customJSON);
	changeLayer("customlayer");
}
function checkCustomURLType(customURL) {
	clearLayers();
	if (customURL.substring(customURL.length-4,customURL.length).toUpperCase() === '.KML' || customURL.substring(customURL.length-4,customURL.length).toUpperCase() === '.KMZ') {
		document.getElementById("addcustomlayer_Type").options[2].selected = true;
		document.getElementById('addcustomlayer_Type').selectedIndex = 2;
		console.log('Layer is a KML based on the URL.');
	}
	if (customURL.substring(customURL.length-9,customURL.length).toUpperCase() === 'MAPSERVER') {
		console.log('Layer is a ArcGIS Layer based on the URL.');
	}
}
