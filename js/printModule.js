// Print Module
dojo.require("esri.dijit.Print");
var printDijit;
function initPrintModule() {
	esri.config.defaults.io.proxyUrl = "/data/proxy.php";
	// print dijit
    printDijit = new esri.dijit.Print({
      map: map,
      url: "http://map.geog.ucsb.edu:8080/arcgis/rest/services/Utilities/PrintingTools/GPServer/Export%20Web%20Map%20Task",
       templates: [{
		    label: "PDF (Landscape)",
		    format: "PDF",
		    layout: "A4 Landscape",
		    preserveScale: false,
		    layoutOptions: {
		      titleText: "University of California, Santa Barbara",
		      authorText: "UCSB Geography Department",
		      copyrightText: "University of California",
		      scalebarUnit: "Miles"
		    }
		  },
		  {
		    label: "PDF (Portrait)",
		    format: "PDF",
		    layout: "A4 Portrait",
		    preserveScale: false,
		    layoutOptions: {
		      titleText: "University of California, Santa Barbara",
		      authorText: "UCSB Geography Department",
		      copyrightText: "University of California",
		      scalebarUnit: "Miles"
		    }
		  },
		  {
		    label: "Export as Image (PNG)",
		    format: "PNG32",
		    layout: "MAP_ONLY",
		    preserveScale: false,
		    exportOptions: {
		      width: 1024,
		      height: 768,
		      dpi: 96
		    }
		  }]
    }, dojo.byId("printButton"));
    printDijit.startup();
    dojo.connect(printDijit,'onPrintComplete',function(value){
	    alert('The printout will open in a new window.  If you don\'t see it, try temporarily disabling your popup blocker.');
	    window.open(value.url,'_blank');
	    printDijit.destroy();
	    initPrintModule();
	  });
};