var hoverGraphicsLayerBus;
var hoverGraphicsLayerFloor;
var hoverGraphicsLayerParking;
var hoverGraphicsLayer;

function initParkingPopup() {
		hoverGraphicsLayerParking = new esri.layers.GraphicsLayer();
		
		//build query filter
		var parkingQueryTask = new esri.tasks.QueryTask("http://map.geog.ucsb.edu:8080/arcgis/rest/services/icm/parkingNumberedLots/MapServer/0");
		var parkingQuery = new esri.tasks.Query();
		parkingQuery.returnGeometry = true;
		parkingQuery.outFields = ["Lot_Number", "T1", "T2","T3","T4","T5","T6","T7","T8","T9","T10","T11","T12","T13","T14","T15","T16"];
		parkingQuery.where = "1=1";
		//parkingQuery.geometry = evt.mapPoint;  //query the clicked point

		var someText = "<b>" + "${T1} "+ "</b><br>" + 
		"<br>Number of Parking Spaces:<br>" +
		"<br>" + "${T2}" + 
		"<br>" + "${T3}" + 
		"<br>" + "${T4}" + 
		"<br>" + "${T5} " + 
		"<br>" + "${T6} " + 
		"<br>" + "${T7} " + 
		"<br>" + "${T8} " + 
		"<br>" + "${T9} " + 
		"<br>" + "${T10} " + 
		"<br>" + "${T11}" + 
		"<br>" + "${T12} " + 
		"<br>" + "${T13} " + 
		"<br>" + "${T14} " +
		"<br>" + "${T15} " +
		"<br>" + "${T16} " +
		"<br><br><small>For official parking information refer to <a href='http://www.tps.ucsb.edu/'> UCSB Transportation and Parking Services.</a></small>";


		var infoTemplate = new esri.InfoTemplate();
		infoTemplate.setTitle("${Lot_Number} ");
		infoTemplate.setContent(someText);

		dojo.connect(parkingQueryTask, "onComplete", function(featureSet4) {
			var symbolParking = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0,0,0,0])), new dojo.Color([0,0,128,0]));
			var highlightSymbolParking = new esri.symbol.SimpleFillSymbol(esri.symbol.SimpleFillSymbol.STYLE_SOLID, new esri.symbol.SimpleLineSymbol(esri.symbol.SimpleLineSymbol.STYLE_SOLID, new dojo.Color([0,0,128]), 2), new dojo.Color([0,0,128,0.4]));

			//QueryTask returns a featureSet.  Loop through features in the featureSet and add them to the map.
			for (var i=0, il=featureSet4.features.length; i<il; i++) {
				//Get the current feature from the featureSet, Feature is a graphic
				var graphic4 = featureSet4.features[i];
				graphic4.setSymbol(symbolParking);
				graphic4.setInfoTemplate(infoTemplate);
				//Add graphic to the floor graphics layer.
				hoverGraphicsLayerParking.add(graphic4);
			}
			map.addLayer(hoverGraphicsLayerParking);
			map.graphics.enableMouseEvents();
			//click on hoverGraphicsLayerParking creates new graphic with geometry from the event.graphic and adds it to the maps graphics layer
			dojo.connect(hoverGraphicsLayerParking, "onClick", function(evt) {
				map.infoWindow.resize(350,250);
				map.graphics.clear();  //use the maps graphics layer as the highlight layer
				var content = evt.graphic.getContent();
				var title = evt.graphic.getTitle();
				var highlightGraphic = new esri.Graphic(evt.graphic.geometry,highlightSymbolParking);
				map.graphics.add(highlightGraphic);
                                if (user_device == 'mobile'  || user_device == 'tablet') {
                                    map.infoWindow.setFeatures([evt.graphic]);       
                                }
			});
			dojo.connect(hoverGraphicsLayerParking, "onMouseOver", function() {
				map.setCursor("pointer");
			});
			dojo.connect(hoverGraphicsLayerParking, "onMouseOut", function() {
				map.setCursor("default");
			});
		});
		parkingQueryTask.execute(parkingQuery);
	}