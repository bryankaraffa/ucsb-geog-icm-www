/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
var geoLocate;
var home;

function initGeolocation(map) {
     	
       require([
       "esri/dijit/LocateButton","esri/dijit/HomeButton",
      "dojo/domReady!"
    ], function( LocateButton, HomeButton )  {
            
      geoLocate = new LocateButton({
        map: map
      }, "LocateButton");
      geoLocate.startup();
      
      home = new HomeButton({
        map: map
      }, "HomeButton");
      home.startup();

    });
        
}
