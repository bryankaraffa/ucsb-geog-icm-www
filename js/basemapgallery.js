var basemapGallery;
var icmBasemap;
function initBasemapGallery() {
	require(["esri/dijit/BasemapGallery", "esri/arcgis/utils", "dojo/parser", "dijit/layout/BorderContainer", "dijit/layout/ContentPane", "dijit/TitlePane", "dojo/domReady!"],
	 function(BasemapGallery, arcgisUtils, parser) {
		parser.parse();
		
		icmBasemap = new esri.dijit.Basemap({
			  layers:[layers['basemap']],
			  title:"UCSB Basemap",
			  thumbnailUrl:"/images/basemapimages/basemap_default.png"
			});
			
		layers['aerialimagery2012'] = initJSONLayer(layersConfig['aerialimagery2012']);
		var basemap2012 = new esri.dijit.Basemap({
			  layers:[layers['aerialimagery2012']],
			  title:"2012 Aerial",
			  thumbnailUrl:"/images/basemapimages/basemap_aerial2012.png"
			});
			
		layers['aerialimagery2010'] = initJSONLayer(layersConfig['aerialimagery2010']);
		var basemap2010 = new esri.dijit.Basemap({
			  layers:[layers['aerialimagery2010']],
			  title:"2010 Aerial",
			  thumbnailUrl:"/images/basemapimages/basemap_aerial2010.png"
			});
			
		layers['aerialimagery2006'] = initJSONLayer(layersConfig['aerialimagery2006']);
		var basemap2006 = new esri.dijit.Basemap({
			  layers:[layers['aerialimagery2006']],
			  title:"2006 Aerial",
			  thumbnailUrl:"/images/basemapimages/basemap_aerial2006.png"
			});	
					
	
		//add the basemap gallery, in this case we'll display maps from ArcGIS.com including bing maps
		basemapGallery = new BasemapGallery({
			showArcGISBasemaps : true,
			map : map,
			basemaps: [icmBasemap,basemap2012,basemap2010,basemap2006],
			showArcGISBasemaps: false
		}, "basemapGallery");
		basemapGallery.startup();
	
		basemapGallery.on("error", function(msg) {
			console.log("basemap gallery error:  ", msg);
		});
		console.log('Basemap gallery loaded!');
	});
} 