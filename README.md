# University of California, Santa Barbara Interactive Campus Map (UCSB ICM)

#### Developed by the [UCSB Department of Geography](http://geog.ucsb.edu)

This is a component of the [`ucsb-geog-icm`](//bitbucket.org/bryankaraffa/ucsb-geog-icm/).  You can visit the master repository for more information.

#### Developed by the [UCSB Department of Geography](http://geog.ucsb.edu)

## Live URL:  [http://map.geog.ucsb.edu/](http://map.geog.ucsb.edu/)


## Getting Started

#### What is this?
This repository (`ucsb-geog-icm-www`) is the source code repository for the UCSB ICM web component.  It can be run on any server that is running an HTTPD and PHP 5.3+.  A lot of the functionality of the this component depends on other components that work together to power the UCSB ICM.  For more information about the entire UCSB ICM system, please refer to the parent repository: [`ucsb-geog-icm`](https://bitbucket.org/bryankaraffa/ucsb-geog-icm)

The latest stable version is in the `master` branch and the latest development version is in the `develop` branch.  

For more information, please contact icm@geog.ucsb.edu 
