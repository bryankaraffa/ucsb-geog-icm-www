<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

return array(
    // 'js' => array('//js/file1.js', '//js/file2.js'),
    'icm-css' => array('//css/simpletree.css'),
    'icm-js' => array('//js/basemapgallery.js','//js/geolocation.js', '//js/main.js', '//js/searchMap.js', '//js/shareLink.js', '//js/infowindowParking.js', '//js/infowindowBuildings.js', '//js/simpletreemenu.js', '//js/layerLoader.js', '//js/addCustomLayer.js','//js/energyData.js','//js/printModule.js'),
);