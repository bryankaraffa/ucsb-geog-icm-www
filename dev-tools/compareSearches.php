<form action="compareSearches.php">
	<label for="searchTerm">Search Term: </label><input name="searchTerm"></input><button type="submit">Search</button><br />
</form>
<?php
function file_get_contents_curl($url) {
    $ch = curl_init();
 
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
 
    $data = curl_exec($ch);
    curl_close($ch);
 
    return $data;
}

if (isset($_REQUEST) && $_REQUEST['searchTerm'] != null) {
	$testSites = array(	
					'Old Search' => 'http://bk.mapdev.geog.ucsb.edu/api.php?action=searchboxSearch-v0&searchText=',
					'New Search' => 'http://bk.mapdev.geog.ucsb.edu/api.php?action=searchboxSearch&searchText='
					);
	$output['categories'] = array();
	$output['siteNames'] = array();
	foreach ($testSites as $name => $site) {
		array_push($output['siteNames'], $name);
		$testURL = $site.urlencode($_REQUEST['searchTerm']);
		//print('<div style="width:50%;float:left">');
		//print('<h1>'.$name.'</h1>');
		$results = json_decode(file_get_contents_curl($testURL), true);
		$output[$name] = $results;
		if ($results['total_count'] > 0) {
			
			foreach ($results['results'] as $catname => $cat) {
				array_push($output['categories'], $catname);
				//print('<div style="width:30%;float:left; padding:3%">');
				//print('<h3>'.$catname.'</h3>');
				foreach($cat as $feature) {
					//print($feature['label'].'<br />');
				}
				//print('<hr />');
				//print('</div>');
			}
			
		}
		else {
			//print('<font style="color:red;">No Results</font>');
		}
		//print('</div>');			
	}
	$output['categories'] = array_unique($output['categories']);
	print('<table border="1" style="">');
	print('<tr><td><h1>Old Search</h1></td><td><h1>New Search</h1></td></tr>');
	$c=0;
	foreach ($output['categories'] as $category) {
		if (is_int($c /2)) { $rowColor = '#DDDDDD'; }
        if (!is_int($c /2)) { $rowColor = 'white'; }
		
		print('<tr style="background:'.$rowColor.'"><td><h3>'.$category.'</h3></td><td><h3>'.$category.'</h3></td></tr>');
		print('<tr style="background:'.$rowColor.'">');
		foreach ($output['siteNames'] as $site) {
			print('<td>');
			if ($output[$site]['results'][$category]['count'] > 0) {
				foreach ($output[$site]['results'][$category] as $key => $feature) {
					if ($feature['label'] != '' && isset($feature['label'])) {
						print($feature['label'].'<br />');
					}	
				}
			}
			else {
				print('<font style="color:red;"><b>No Results</b></font>');
			}
			print('</td>');
		}
		print('</tr>');
		$c++;
	}
	print('</table>');
	print('
	
	
	
	Raw Output:<br /><pre>');
	print_r($output);
	
}
?>